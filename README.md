# Ansible Role: PentOS Repository

Installs the PentOS repo providing RPMs for PentOS pentesting overlay on CentOS/RHEL/Fedora. Modified from original for epel b y geerlingguy

NONE OF THIS IS READY TO RUN

## Requirements

This role only is needed/runs on RHEL and its derivatives.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    pentos_repo_url: todo
    epel_repo_gpg_key_url: todo

The EPEL repo URL and GPG key URL. Generally, these should not be changed, but if this role is out of date, or if you need a very specific version, these can both be overridden.

## Dependencies

None.

## Example Playbook

    - hosts: servers
      roles:
        - geerlingguy.repo-pentos

## License

MIT / BSD

## Author Information

This role was modified in 2018 from a similar role for epel written in 2014 by [Jeff Geerling](https://www.jeffgeerling.com/), author of [Ansible for DevOps](https://www.ansiblefordevops.com/). There is no association with Jeff Geerling or his projects.
